###########################################################################################
# Name: Brennan Forrest
# Date: 10/13/17
# Description: Easy Doing it but reloaded this time
###########################################################################################

#get_name (no args): prompts the user for a name. Basically a proxy for the raw_input function. 
def get_name():
	return raw_input("Please enter your name: ")

#get_age (name): prompts the user for their age with a kind nod.
def get_age(name):
	return input("How old are you, {}? ".format(name))

def swag_print(name, age):
	print("Hi, {}. You are {} years old. Twice your age is {}.".format(name, age, age * 2))


###############################################
# MAIN PART OF THE PROGRAM
# implement the main part of your program below
# comments have been added to assist you
###############################################
# prompt for a name
name = get_name()

# prompt for an age
age = get_age(name)

# display the final output
swag_print(name, age)
